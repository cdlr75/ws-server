WS Server [DOC](https://cdlr75.gitlab.io/ws-server/)
===

Playground around websockets.

Install using pip: `pip install sample-ws-server`

```
$ ws_server -h
usage: __main__.py [-h] [--host HOST] [--port PORT] [--verbosity VERBOSITY]

optional arguments:
  -h, --help            show this help message and exit
  --host HOST           Server host
  --port PORT           Port to bind
  --verbosity VERBOSITY
                        Log level DEBUG|INFO|WARNING...
```
