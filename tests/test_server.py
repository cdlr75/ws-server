""" Test a web socket server implementation. """
import asyncio
from contextlib import suppress
import websockets
import asynctest
from ws_server.server import Server


class TestServer(asynctest.TestCase):
    """ Server testing class. """

    async def setUp(self):
        self.port = 5000
        self.server = await Server.create(port=self.port)

    async def tearDown(self):
        self.server.close()
        await self.server.wait_closed()

    async def test_single_client_connection(self):
        """ Connection Server and send a message. """
        self.assertEqual(len(self.server.sockets), 0)
        async with websockets.connect(f'ws://127.0.0.1:{self.port}') as websocket:
            # one client, no message
            await asyncio.sleep(0.1)
            self.assertEqual(len(self.server.sockets), 1)
            self.assertEqual(len(self.server.sockets[1]["msg"]), 0)
            # When
            await websocket.send("hello")
            # Then
            await asyncio.sleep(0.1)
            # one message
            self.assertEqual(self.server.sockets[1]["msg"], ["hello"])

    async def test_mutliple_client_connections(self):
        """ 5 client sending their id are managed be the server. """

        async def make_connection(close_event, connected_event, client_id):
            """ Connect to Server. """
            async with websockets.connect(f'ws://127.0.0.1:{self.port}') as websocket:
                connected_event.set()
                await websocket.send(client_id)
                await close_event.wait()

        # When making 5 connections
        close_event = asyncio.Event()
        connections = []
        for client_id in range(5):
            connected_event = asyncio.Event()
            connections.append(
                asyncio.ensure_future(make_connection(
                    close_event, connected_event, str(client_id)))
            )
            await connected_event.wait()
        # Then server is holding 5 connections
        await asyncio.sleep(0.1)
        self.assertEqual(len(self.server.sockets), 5)
        self.assertEqual([sock["msg"][0] for sock in self.server.sockets.values()],
                         [str(x) for x in range(5)])
        # stop clients
        close_event.set()
        await asyncio.gather(*connections)

    async def test_send_msg_to_client(self):
        """ Send a message to a client. """

        async def client(started_event):
            """ A dummy client waiting and returning first server msg. """
            async with websockets.connect(f'ws://127.0.0.1:{self.port}') as websocket:
                started_event.set()
                message = await websocket.recv()
            return message

        # Given
        started_event = asyncio.Event()
        client_task = asyncio.ensure_future(client(started_event))
        await started_event.wait()
        # When
        await self.server.send_to_clients("hello")
        # Then
        msg = await client_task
        self.assertEqual(msg, "hello")

    async def test_send_msg_with_disconnected_clients(self):
        """ Old clients are not making server in trouble. """
        # Given
        async with websockets.connect(f'ws://127.0.0.1:{self.port}') as websocket:
            pass
        self.assertEqual(len(self.server.sockets), 1)

        await self.server.send_to_clients("hello")
