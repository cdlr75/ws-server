.. ws_server documentation master file, created by
   sphinx-quickstart on Sun Sep 22 22:10:40 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ws_server's documentation!
=====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   ws_server


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
